<?php

namespace Omnipay\PayU\Message\Requests;

use Omnipay\PayU\Message\Responses\PurchaseResponse;

/**
 * Class PurchaseRequest
 * @package Omnipay\PayU\Message\Requests
 */
class PurchaseRequest extends AbstractRequest
{
    /**
     * @var string
     */
    public $testEndpoint = 'https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu/'; // endPoint Payu, you must change  $data['test'] = 'TRUE'; 
    public $prodEndpoint = 'https://checkout.payulatam.com/ppp-web-gateway-payu/';  
    public $endpoint = null;
    /**
     * @return mixed
     */

    
   function __construct($httpClient, $httpRequest) {    
        parent::__construct($httpClient, $httpRequest);
        $this->endpoint = config('attendize.payment_test') ? $this->testEndpoint :  $this->prodEndpoint;
   }

    public function getMerchantName()
    {
        return $this->getParameter('merchantName');
    }

    /**
     * @return mixed
     */
    public function getAccountId()
    {
        return $this->getParameter('accountId');
    }    
    /**
     * @param $value
     * @return $this
     */
    public function setMerchantName($value)
    {
        return $this->setParameter('merchantName', $value);
    }

    /**
     * @return mixed
     */
    public function getOrderDate()
    {
        return $this->getParameter('orderDate');
    }

    /**
     * @param $value
     * @return $this
     */
    public function setOrderDate($value)
    {
        return $this->setParameter('orderDate', $value);
    }

    /**
     * @return mixed
     */
    public function getOrderTimeout()
    {
        return $this->getParameter('orderTimeout');
    }

    /**
     * @param $value
     * @return $this
     */
    public function setOrderTimeout($value)
    {
        return $this->setParameter('orderTimeout', $value);
    }

           /**
     * @deprecated
     */
    public function getEmail()
    {
        return $this->getParameter('email');
    }

    public function setEmail($value)
    {
        return $this->setParameter('email', $value);
    }
           /**
     * @deprecated
     */
    public function getParametro()
    {
        return $this->getParameter('ApiKey');
    }

    public function setParametro($value)
    {
        return $this->setParameter('ApiKey', $value);
    }

    /**
     * @return mixed
     * @throws \Omnipay\Common\Exception\InvalidRequestException
     */
    public function getData()
    {
        $this->validate('transactionId', 'merchantName', 'orderDate', 'items');
        if(config('attendize.payment_test')){
            $data['merchantId'] = '508029';
            $data['accountId'] = "512321";
            $data['apikey'] = '4Vj8eK4rloUd272L48hsrarnUA';
            $data['test'] = 1;
        }else{
            $data['merchantId'] = $this->getMerchantName();
            $data['accountId'] = "588020";//$this->getAccountId();
            $data['apikey'] = 'omF0uvbN3365dC2X4dtcjywbS7';
            $data['test'] = 'FALSE';
        }

        $data['referenceCode'] = $this->getTransactionId();
        $data['tax'] = 0;
        $data['taxReturnBase'] = 0;
        $data['BACK_REF'] = $this->getReturnUrl();
        $data['buyerEmail'] = $this->getEmail();
        
        foreach ($this->getItems() as $item) {
            $item->validate();

            $data['ORDER_PNAME'] = $item->getName();
            $data['ORDER_PCODE'] = $item->getCode();
            $data['descripcion'] = $item->getDescription();
            $data['valor'] = $item->getPrice();
            $data['responseUrl'] = $item->getUrl();
            $data['confirmationUrl'] = $item->getconfirmationUrl();
            $data['ORDER_QTY'] = $item->getQuantity();
            $data['ORDER_VAT'] = $item->getVat();
        }

        $data['currency'] = $this->getCurrency();
        $data['PAY_METHOD'] = $this->getPaymentMethod();

        foreach ($this->getItems() as $key => $item) {
            $data['ORDER_PRICE_TYPE[' . $key . ']'] = $item->getPriceType();
        }

        if ($card = $this->getCard()) {
            $data['BILL_LNAME'] = $card->getBillingLastName();
            $data['BILL_FNAME'] = $card->getBillingFirstName();
            $data['BILL_EMAIL'] = $card->getEmail();
            $data['BILL_PHONE'] = $card->getBillingPhone();
            $data['BILL_COUNTRYCODE'] = $card->getBillingCountry();
        }
        if ($this->getTestMode()) {
            $data['test'] = 'TRUE';
            $data['TESTORDER'] = 'TRUE';
        }


        $data = $this->filterNullValues($data);

        $data_hash = [
            $data['apikey'],
            $data['merchantId'],
            $data['referenceCode'],
            $data['valor'],
            $data['currency']
        ];

        $data['signature'] = $this->generateHash($data_hash);
        return $data;
    }

    /**
     * @param array $data
     * @return array
     */
    protected function filterNullValues(array $data)
    {
        return array_filter($data, function ($value) {
            return !is_null($value);
        });
    }

    /**
     * @param mixed $data
     * @return \Omnipay\Common\Message\ResponseInterface|PurchaseResponse
     */
    public function sendData($data)
    {
        return new PurchaseResponse($this, $data);
    }
}